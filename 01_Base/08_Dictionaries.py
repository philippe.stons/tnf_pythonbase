dico = { "entry_1": 153, "entry_2": "test", "entry_3": 12.5 }
print(dico["entry_1"])
print(dico["entry_2"])
print(dico["entry_3"])
dico["entry_4"] = 253
print(dico["entry_4"])

for i in dico.items():
    print(i)

for key, val in dico.items():
    print("key : ", key, " val : ", val)