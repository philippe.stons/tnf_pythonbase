#singleton
instance = []

class Cat:
    @staticmethod
    def staticMethod():
        instance.append("test")
        print("this method is static!")

Cat.staticMethod()

print(instance)