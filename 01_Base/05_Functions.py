def testFunction():
    print("test function")

def testArgFunc(agr_1, arg_2):
    for i in range(agr_1):
        print(arg_2)

def testRtnArg(agr_1, arg_2):
    return agr_1 * arg_2

a = 5
b = "my super string!"
testArgFunc(a, b)
testFunction()
c = testRtnArg(a, 10)
print(c)