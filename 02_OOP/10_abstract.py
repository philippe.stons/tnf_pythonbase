from abc import ABC, abstractmethod

class AbstractClass(ABC):

    @abstractmethod
    def abstractMethod(self, msg):
        pass
    
class Derived(AbstractClass):
    def abstractMethod(self, msg):
        print(msg)

    
test = Derived()
test.abstractMethod("message")